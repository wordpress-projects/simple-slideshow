<?php
/*
Plugin Name: Silencesoft Simple Slideshow
Plugin URI: http://www.silencesoft.co
Description: A plugin to create slideshow from a folder
Version: 0.1
Author: Byron Herrera
Author URI: http://byronh.axul.net
*/


$sil_slideshow = new sil_simple_slideshow();
// $sil_slideshow->init();
add_action( 'init', array($sil_slideshow, 'init' ), 1 );

add_filter('the_content', array($sil_slideshow, 'sil_slideshow_content'));
// add_action('wp_print_scripts', array($sil_slideshow, 'sil_load_scripts'));

add_action('init', array($sil_slideshow, 'sil_register_scripts'));
add_action('wp_footer', array($sil_slideshow, 'sil_print_script'));

add_action( 'wp_head', array( $sil_slideshow, 'sil_print_styles'), 1 );

add_action('wp_ajax_sil_slideshow', array( $sil_slideshow, 'sil_slideshow_callback'), 1 );
add_action('wp_ajax_nopriv_sil_slideshow', array( $sil_slideshow, 'sil_slideshow_callback'), 1 );


class sil_simple_slideshow
{
	var $add_code;
	var $plugin_url;
	var $imagetypes;
	var $js;
	var $path;
	var $per_page;

	function sil_simple_slideshow()
	{
		$this->init;
	}

	function init()
	{
		$this->imagetypes = array("image/jpeg", "image/gif");
		$this->add_code = 0;
		$this->plugin_url = plugins_url() . "/". plugin_basename( dirname( __file__ ) );
	}

	function sil_print_styles()
	{
		wp_register_style( 'sil-slideshow-gallery-style', plugins_url('css/jquery.galleryview-3.0-dev.css', __FILE__) );
		wp_enqueue_style( 'sil-slideshow-gallery-style' );
		wp_register_style( 'sil-slideshow-style', plugins_url('css/sil_simple_slideshow.css', __FILE__) );
		wp_enqueue_style( 'sil-slideshow-style' );
		// echo '<link type="text/css" rel="stylesheet" href="' .$this->plugin_url . '/css/jquery.galleryview-3.0-dev.css" />' . "\n";
		// echo '<link type="text/css" rel="stylesheet" href="' .$this->plugin_url . '/css/sil_simple_slideshow.css" />' . "\n";
	}

	function sil_load_scripts()
	{
		if ($this->add_code)
		{
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-slider');
			wp_enqueue_script('jquery_timers', $this->plugin_url . "/js/jquery.timers-1.2.js", array('jquery'));
			wp_enqueue_script('jquery_easing', $this->plugin_url . "/js/jquery.easing.1.3.js", array('jquery'));
			wp_enqueue_script('jquery_galleryview', $this->plugin_url . "/js/jquery.galleryview-3.0-dev.js", array('jquery'));
			wp_enqueue_script('sil_simple_slideshow', $this->plugin_url . "/js/sil_simple_slideshow.js", array('jquery'));

		}
	}

	function sil_register_scripts()
	{
		// wp_register_script('jquery');
		// wp_register_script('jquery-ui-core');
		// wp_register_script('jquery-ui-slider');
		wp_register_script('jquery_timers', $this->plugin_url . "/js/jquery.timers-1.2.js", array('jquery'));
		wp_register_script('jquery_easing', $this->plugin_url . "/js/jquery.easing.1.3.js", array('jquery'));
		wp_register_script('jquery_galleryview', $this->plugin_url . "/js/jquery.galleryview-3.0-dev.js", array('jquery'));
		wp_register_script('sil_simple_slideshow', $this->plugin_url . "/js/sil_simple_slideshow.js", array('jquery'));
		wp_localize_script( 'sil_simple_slideshow', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	}

	function sil_print_script() {
		if (!$this->add_code)
		return;

		// wp_print_scripts('jquery');
		// wp_print_scripts('jquery-ui-core');
		// wp_print_scripts('jquery-ui-slider');
		wp_print_scripts('jquery_timers');
		wp_print_scripts('jquery_easing');
		wp_print_scripts('jquery_galleryview');
		wp_print_scripts('sil_simple_slideshow');
		echo $this->js;
	}

	function sil_slideshow_content($content)
	{
		$sil_tag = "[sil_slideshow ";
		$start = stripos($content, $sil_tag, 0);
		if ($start)
		{
			$this->add_code = 1;
			$end = stripos($content, "]", $start);
			$sil_text = substr($content, $start, $end - $start + 1);
			$values += $start + strlen($sil_tag);
			$sil_data = substr($content, $values, $end - $values);
			$sil_array = explode(" ", $sil_data);
			$this->js = "<script type=\"text/javascript\">
			jQuery(function(){
			sil_load_gallery = function() {
			jQuery('#sil_gallery').galleryView({\n";
			$output = "<div id=\"sil_simple_slideshow\">\n<div id=\"sil_slideshow_content\">\n<ul id=\"sil_gallery\">\n";
			$pagination =  "";
			$html_images =  "";
			$h = 0;
			$use_lightbox = true;

			foreach ($sil_array as $d => $sil_value)
			{
				list($name, $value) = explode("=", $sil_value);
				$value = str_replace("\"", "", $value);
				if ($name == "folder")
				{
					// print rtrim(dirname(__FILE__), '/\\') . DIRECTORY_SEPARATOR . 'my_file.php';
					// print_R($this->sil_get_images(dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.$value));
					$this->path = $value;
					$images = $this->sil_get_images($value);
					foreach($images as $img) {
						// echo "<img class=\"photo\" src=\"{$img['file']}\" {$img['size'][3]} alt=\"\">\n";
						$html_images .= "<li><img src=\"{$img['file']}\" alt=\"{$img['name']}\" /></li>\n";
					}
				}
				else if ($name == "per_page")
				{
					$sil_tmp = explode("\n", $html_images);
					if (count($sil_tmp) > (int)$value)
					{
						$this->per_page = $value;
						$html_images = "";
						for ($c = 0; $c < (int)$value; $c++)
						{
							$html_images .= $sil_tmp[$c]."\n";
						}
						$page_total = ceil(count($sil_tmp) / (int)$value);
						$pagination =  "\n<ul id=\"sil_pagination\">";
						for ($c = 0; $c < $page_total; $c++)
						{
							$pagination .= "<li id=\"sil_page_".($c+1)."\" class=\"sil_page".(($c==0)?" sil_page_active":"")."\"><a href=\"#\">".($c+1)."</a></li>";
						}
						$pagination .=  "</ul>";
					}
				}
				else if ($name == "use_lightbox")
				{
					$use_lightbox = $value;
				}
				else
				{
					$this->js .= $name.":".$value.",\n";
				}

			}
			$this->js .= "
			});

			}
			sil_load_gallery();
			jQuery('#sil_slideshow_content').height(jQuery('.gv_galleryWrap').height()+5);
			jQuery('#sil_slideshow_content').width(jQuery('.gv_galleryWrap').width()+5);
			jQuery('#sil_slideshow_content').css('background-color',  jQuery('.gv_galleryWrap').css('background-color'));

			jQuery('ul#sil_pagination li a').click(function(event) {
			//   alert('clicked: ' + event.target.id);
			//			jQuery('ul#sil_pagination li').removeClass('sil_page_active');
			var target = jQuery(event.target);
			var parent = target.parent()
			var id = parent.attr('id');
			var siblings = parent.siblings();
			var isOn = parent.toggleClass('sil_page_active').hasClass('sil_page');
			siblings.toggleClass('sil_page_active', !isOn);

			//    parent.addClass('sil_page_active');

			sil_GoToPage(id, '".$this->path."', '".$this->per_page."');
			return false;
			});

			jQuery('ul#sil_pagination li').click(function(event) {
			//			jQuery('ul#sil_pagination li').each(function(event) {
			// alert('clicked: ' + event.target.nodeName);
			jQuery('ul#sil_pagination li').removeClass('sil_page_active');
			var target = jQuery(event.target);
			if(!target.is('li')) return;
			target.addClass('sil_page_active');
			var launch = jQuery('a', this);
			//			if (launch.size() > 0) { this.onclick = launch.attr('onclick'); }
			//    if (launch.size() > 0) { eval(launch.attr('onclick')) }
			if (launch.size() > 0) { eval(launch.click()) }
			var siblings = target.siblings();
			var isOn = target.toggleClass('sil_page_active').hasClass('sil_page');
			siblings.toggleClass('sil_page_active', !isOn);
			});
			if (use_lightbox)
			{
			jQuery('.gv_panelWrap').css('cursor','pointer');
			jQuery('.gv_panelWrap').click(function(event) {
			event.preventDefault();
			sil_loadLightbox();
			return false;
			});
			}

			//Click anywhere on the page to get rid of lightbox window
			jQuery('#sil_lightbox').live('click', function() { //must use live, as the lightbox element is inserted into the DOM
			jQuery('#sil_lightbox').hide();
			});

			});
			</script>";
			$this->js = "<script type=\"text/javascript\">
			var use_lightbox = ".$use_lightbox.";
			</script>\n".$this->js;
			$output .= $html_images."	</ul></div>".$pagination."\n<div style=\"clear:both;\"></div>\n</div>";
			$content = str_replace($sil_text, $output, $content);

		}
		return $content;
	}

	function sil_get_images($dir)
	{

		$images = array();

		// $fulldir = "{$_SERVER['DOCUMENT_ROOT']}/$dir";
		// $fulldir = "$dir";
		$fulldir = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.str_replace("/", DIRECTORY_SEPARATOR, $dir);

		if(substr($dir, -1) != "/")
		{
			$dir .= "/";
			$fulldir .= DIRECTORY_SEPARATOR;
		}

		$d = @dir($fulldir) or die("sil_get_images: Failed opening directory $dir for reading");
		while(false !== ($entry = $d->read())) {
			// skip hidden files
			if($entry[0] == ".") continue;

			// check for image files
			if (function_exists('exec'))
			$f = escapeshellarg("$fulldir$entry");
			$ext = pathinfo($entry, PATHINFO_EXTENSION);
			if ($ext == "JPG" || $ext == "jpg")
			$images[] = array(
			'file' => plugins_url() ."/../$dir$entry",
			'name' => $entry,
			'size' => getImagesize("$fulldir$entry")
			);

		}
		$d->close();
		$fileArray = array(); 
		foreach($images as $file){
			foreach($file as $key=>$value){
				if(!isset($fileArray[$key])){
					$fileArray[$key] = array();
				}
				$fileArray[$key][] = $value;
			}
		}
		array_multisort($fileArray['name'],SORT_ASC,$images);
		return $images;
	}


	function sil_slideshow_callback() {

		$page = intval( str_replace("sil_page_", "", $_POST['load']));
		$folder = ( $_POST['folder'] );
		$value = intval( $_POST['total']  );

		$output = "<ul id=\"sil_gallery\">";
		$images = $this->sil_get_images($folder);
		foreach($images as $img) {
			$html_images .= "<li><img src=\"{$img['file']}\" alt=\"{$img['name']}\" /></li>\n";
		}
		$sil_tmp = explode("\n", $html_images);

		if (count($sil_tmp) > $value)
		{
			$html_images = "";
			$imgs = ($page-1)*$value;
			for ($c = $imgs; $c < $imgs+$value; $c++)
			{
				$html_images .= $sil_tmp[$c]."\n";
			}
		}

		$output .= $html_images."</ul>";

		echo $output;
		die();
	}

}



