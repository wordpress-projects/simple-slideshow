function sil_GoToPage(page, path, number)
{
	jQuery('#sil_slideshow_content').empty();
	// jQuery("#randomdiv").load("load.php")
	jQuery('#sil_slideshow_content').prepend('<img id="sil_loading_image" src="./wp-content/plugins/sil_simple_slideshow/css/img-loader.gif" />')
	jQuery('#sil_loading_image').css('margin-top', '50%');
	jQuery('#sil_loading_image').css('margin-left', '50%');

	var data = {
		action: 'sil_slideshow',
		load: page,
		folder: path,
		total: number
	};

	jQuery.post(MyAjax.ajaxurl, data, function(response) {
		// alert('Got this from the server: ' + response);
		jQuery('#sil_loading_image').remove();
		jQuery('#sil_slideshow_content').html(response);
		sil_load_gallery();
			if (use_lightbox)
			{

			jQuery('.gv_panelWrap').css('cursor','pointer');
			jQuery('.gv_panelWrap').click(function(event) {
			event.preventDefault();
			sil_loadLightbox();
			return false;
			});
		}

	});

	return false;
}

// Based on 
// Super Simple Lightbox with CSS and jQuery
// Jim Nielsen
// http://webdesign.tutsplus.com/tutorials/htmlcss-tutorials/super-simple-lightbox-with-css-and-jquery/
function sil_loadLightbox()
{

	var img = jQuery('.gv_panel img');
	var img_src = img.attr('src');
	// alert('cosa '+img_src);

	/*
	If the lightbox window HTML already exists in document,
	change the img src to to match the href of whatever link was clicked

	If the lightbox window HTML doesn't exists, create it and insert it.
	(This will only happen the first time around)
	*/

	if (jQuery('#sil_lightbox').length > 0) { // #sil_lightbox exists

		//place href as img src value
		jQuery('#sil_l_content').html('<img src=\"' + img_src + '\" />');

		//show lightbox window - you could use .show('fast') for a transition
		jQuery('#sil_lightbox').show();
	}

	else { //#sil_lightbox does not exist - create and insert (runs 1st time only)

		//create HTML markup for lightbox window
		var lightbox =
		'<div id=\"sil_lightbox\">' +
		'<p>Click to close</p>' +
		'<div id=\"sil_l_content\">' + //insert clicked link's href into img src
		'<img src=\"' + img_src +'\" />' +
		'</div>' +
		'</div>';

		//insert lightbox HTML into page
		jQuery('body').append(lightbox);
	}

}

