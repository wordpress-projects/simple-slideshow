=== Silencesoft Simple Slideshow ===
by: Byron Herrera - bh at silencesoft dot co
Donate link: http://silencesoft.co/Wordpress
Tags: images, slideshow
Requires at least: 3
Tested up to: 3.2.1
Stable tag: 0.5

A simple slideshow plugin.

== Description ==

Why another slideshow plugin?
Well, it's simple. I need a plugin without databases and without a big gallery.
This plugin only takes one parameter: images folder.
After this, it creates slideshow on post page.

To use it add the tag sil_slideshow to your post at this way:
[sil_slideshow folder="path-to-folder"]
Folder must be inside wp-content folder, example: "uploads/my-images"
url for this example is "http://my-blog/wp-content/uploads/my-images"

Other script parameters:
per_page="10" - number of images per page. Predetermined: All images.
use_lightbox="true" - If use a simple lightbox view. Pred: true

This plugins uses "Gallery View" from Jack Wanders 3.0-dev version
http://www.spaceforaname.com/galleryview/
A jQuery plugin that helps to create an image viewer with slideshow.
You can review all parameters of script on its page.

To pass parameters to script add them to the tag, for example:

[sil_slideshow folder="path-to-folder" frame_height="32" frame_width="52" panel_width="800" panel_height="600"]

Note:
-----
I worked on this plugin without too much time, so there is dirty code on it.
You can contact me for help and for fixes and bugs.
Thanks for your support.

Not tested with previous wordpress 3 versions.

Thanks
------
* Jack Wanders
for Gallery View
http://www.spaceforaname.com/galleryview/

* Jim Nielsen 
Article: Super Simple Lightbox with CSS and jQuery
http://webdesign.tutsplus.com/tutorials/htmlcss-tutorials/super-simple-lightbox-with-css-and-jquery/

== Installation ==

To install it:
Download zip file and decompress it.
Upload to your plugins directory.
Enable it.

To use it:
In a post add this tag
[sil_slideshow folder="path-to-folder"]
folder must be inside wp-content folder, example: "uploads/my-images"

== Changelog ==

= 0.5 =
* First release.


